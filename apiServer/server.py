from flask import Flask, request, render_template, make_response
import sys
sys.path.append('./APIamazon')
sys.path.append('./APItwitter')
sys.path.append('./ASUM')
sys.path.append('./apiServer')
from core_extract_comments import get_comments_based_on_keyword
from scraper_tweets import scraper
from pre_process import preprocess
from pandas.io.json import json_normalize
from subprocess import Popen
from post_process import operations
from post_process import getInfoWord
from collections import defaultdict
import numpy as np
from graph_creation import *


app = Flask(__name__, template_folder="../template", static_folder="../template")
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0

def recDict():
    return defaultdict(recDict)

@app.route('/get-text', methods=['GET', 'POST'])
def entryPoint():
    global keyword
    global cols
    global prov
    global reviews
    global results
    global tweet_control
    global amazon_control
    tweet_control =''
    amazon_control=''
    reviews=[]
    rev={}
    results={}
    amazon_control=''
    tweet_control=''
    keyword = request.form.get('keyword')
    prov = request.form.get('provider')
    number = request.form.get('quantity')
    if prov=="amazon":
        reviews,code= get_comments_based_on_keyword(keyword, int(number))
        rev["amazon"]=reviews

    elif prov=="twitter":
        reviews,code, _= scraper(keyword, int(number))
        rev["twitter"] = reviews
    else:
        number_temp=int(int(number)/2)
        rewA,code_a= get_comments_based_on_keyword(keyword, number_temp)
        if rewA != []:
            rev["amazon"] = rewA
            reviews=rewA.copy()
        rewB,code_t, _= scraper(keyword, number_temp)
        if rewB != []:
            rev["twitter"] = rewB
            for r in rewB:
                reviews.append(r)
        rev["all"] = reviews


    if prov != 'all':
        if code==1:
            # da rifare
            return render_template('page_error.html')
            #response = make_response('No reviews found!', 200)
            #return response
            # return html page and show no reviews found!
    for k in rev:
        providerRev = rev[k]
        wList, indexes= preprocess(providerRev)
        if wList!=[]:
            proc = Popen("java -jar ./ASUM/asum-master/asum-1.0.1-SNAPSHOT.jar", shell=True)
            proc.wait()
            proc.kill()
            results[k] = operations()
            indexes.sort(reverse=True)
            for ind in indexes:
                del reviews[ind]
    for i in range(len(reviews)):
        reviews[i]['index'] = i+1
        reviews[i]['positivity'] = int(round(results[prov]['global_sentiment_per_tweet'][i], 2) * 100)
    cols = np.array(('index','provider','content'))

    if 'all' in results.keys():
        values_all = results['all']
        name_graph = create_graph(prov, values_all['global_sentiment_per_tweet'])
        number_elements = len(values_all['global_sentiment_per_tweet'])
        values_all = int(round(np.mean(values_all['global_sentiment_per_tweet']),2)*100)
        if 'twitter' in results.keys():
            tweet_control ='ok'
        if 'amazon' in results.keys():
            amazon_control = 'ok'
        return render_template('select_view.html', key=keyword, pvd = prov, sentiment=values_all, tc = tweet_control, ac =amazon_control, graph = name_graph, num = number_elements)
    elif 'twitter' in results.keys() and not('all' in results.keys()):
        if wList==[]:
            return render_template('page_error.html')
            #response = make_response('No reviews found!', 200)
            #return response
        values_tweets=results["twitter"]
        name_graph = create_graph(prov,values_tweets['global_sentiment_per_tweet'])
        number_elements = len(values_tweets['global_sentiment_per_tweet'])
        values_tweets = int(round(np.mean(values_tweets['global_sentiment_per_tweet']),2)*100)
        return render_template('select_view.html', key=keyword, pvd=prov, sentiment=values_tweets, graph = name_graph, num = number_elements)
    elif 'amazon' in results.keys() and not('all' in results.keys()):
        if wList==[]:
            return render_template('page_error.html')
            #response = make_response('No reviews found!', 200)
            #return response
        values_amazon = results['amazon']
        name_graph = create_graph(prov, values_amazon['global_sentiment_per_tweet'])
        number_elements = len(values_amazon['global_sentiment_per_tweet'])
        values_amazon = int(round(np.mean(values_amazon['global_sentiment_per_tweet']),2)*100)
        return render_template('select_view.html', key=keyword, pvd = prov, sentiment=values_amazon, graph = name_graph, num = number_elements)

@app.route('/<page_name>')
def other_page(page_name):
	return render_template('404page.html')

@app.route('/view_data')
def view_mid():
    global keyword
    global cols
    global prov
    global reviews
    global tweet_control
    global amazon_control
    return render_template('view_data.html', key=keyword, pvd = prov, records=reviews, colnames=cols, tc = tweet_control, ac =amazon_control)

@app.route('/view1')
def view_mid1():
    global keyword
    global prov
    global reviews
    global tweet_control
    global amazon_control
    cols = np.array(('index', 'provider','positivity', 'content'))

    return render_template('view_data1.html', key=keyword, pvd = prov, records=reviews, colnames=cols, tc = tweet_control, ac =amazon_control)

@app.route('/view2', methods=['GET', 'POST'])
def view_mid2():
    global results
    word = request.values.get('word_sel')
    provider_sel = request.values.get('provider_sel')
    # word total in all tweets, num tweet has word, total_tweets, total_words
    infoWord = getInfoWord(word, results[provider_sel]["tweets_word"], len(results[provider_sel]["global_sentiment_per_tweet"]))
    if infoWord != None:
        name_pie = create_pie_all(provider_sel, infoWord, word)
        name_pie_word = create_pie_word(provider_sel, infoWord, word)
        return render_template('view_data2.html', key=word, pvd=provider_sel, graph_all=name_pie, graph_word=name_pie_word, pos=int(infoWord[4]*100))
    else:
        return make_response('No word found in '+provider_sel+'!', 200)


@app.route('/amazon_single')
def amazon_single():
    global keyword
    global results
    if 'amazon' in results.keys():
        values_amazon = results['amazon']
        name_graph = create_graph('amazon', values_amazon['global_sentiment_per_tweet'])
        values_amazon = int(round(np.mean(values_amazon['global_sentiment_per_tweet']),2)*100)
        return render_template('amazon_single.html', key=keyword, sentiment=values_amazon, graph = name_graph)


@app.route('/twitter_single')
def twitter_single():
    global keyword
    global results
    if 'twitter' in results.keys():
        values_tweets = results['twitter']
        name_graph = create_graph('twitter', values_tweets['global_sentiment_per_tweet'])
        values_tweets = int(round(np.mean(values_tweets['global_sentiment_per_tweet']),2)*100)
        return render_template('twitter_single.html', key=keyword, sentiment=values_tweets, graph = name_graph)

@app.route('/')
def home():
    return render_template('index.html')

if __name__ == '__main__':
    app.run()