import os
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go

def create_pie_all(prov,data, word):
    if prov=="all":
        labels = ['Total presence of word ' + word + ' in twitter and amazon', 'Rest of words']
    else:
        labels = ['Total presence of word ' + word + ' in '+prov+' provider', 'Rest of words']
    true_element = data[0]
    false_element = data[3] - data[0]
    values = [true_element, false_element]

    fig = go.Figure(data=[go.Pie(labels=labels, values=values)])
    fig.update_layout({'plot_bgcolor': 'rgba(0, 0, 0, 0)', 'paper_bgcolor': 'rgba(0, 0, 0, 0)'})
    if prov=="all":
        fig.update_layout(
            title={
                'text': "Presence of the word " + word + "\n in twitter and amazon",
                # 'y': 0.9,
                'x': 0.5,
                'xanchor': 'center',
                'yanchor': 'top'},

            font=dict(
                family="san-serif, bold",
                size=20,
                color="#ffffff"
            ))
        fig.update_layout(
            legend=dict(
                x=0.41,
                y=-0.1,
                traceorder="normal",
                font=dict(
                    family="sans-serif",
                    size=18,
                    color="white"
                )
            )
        )
    else:
        fig.update_layout(
            title={
                'text': "Presence of the word " + word + " in "+prov+' provider',
                # 'y': 0.9,
                'x': 0.5,
                'xanchor': 'center',
                'yanchor': 'top'},

            font=dict(
                family="san-serif, bold",
                size=20,
                color="#ffffff"
            ))
        fig.update_layout(
            legend=dict(
                x=0.41,
                y=-0.1,
                traceorder="normal",
                font=dict(
                    family="sans-serif",
                    size=18,
                    color="white"
                )
            )
        )
    fig.update_layout(legend_orientation="h")
    filePath = './template/resource/' + prov + '_pie_positivity.html'
    if os.path.exists(filePath):
        os.remove(filePath)

    fig.write_html("./template/resource/" + prov + "_pie_positivity.html")
    return "./template/resource/" + prov + "_pie_positivity.html"

def create_pie_word(prov,data, word):
    if prov == "all":
        labels = ['Instances of all providers with word '+word, 'Rest of instances']
    else:
        labels = ['Instances of '+prov+' provider with word ' + word, 'Rest of instances']
    true_element = data[1]
    false_element = data[2] - data[1]
    values = [true_element, false_element]

    fig = go.Figure(data=[go.Pie(labels=labels, values=values)])
    fig.update_layout({'plot_bgcolor': 'rgba(0, 0, 0, 0)', 'paper_bgcolor': 'rgba(0, 0, 0, 0)'})
    if prov == "all":
        fig.update_layout(
            title={
                'text': "Instances of providers with at leas one time word "+word,
                #'y': 0.9,
                'x': 0.5,
                'xanchor': 'center',
                'yanchor': 'top'},

            font=dict(
                family="san-serif, bold",
                size=20,
                color="#ffffff"
            ))
        fig.update_layout(
            legend=dict(
                x=0.41,
                y=-0.1,
                traceorder="normal",
                font=dict(
                    family="sans-serif",
                    size=18,
                    color="white"
                )
            )
        )
    else:
        fig.update_layout(
            title={
                'text': prov+" provider instances with at leas one time word " + word,
                # 'y': 0.9,
                'x': 0.5,
                'xanchor': 'center',
                'yanchor': 'top'},

            font=dict(
                family="san-serif, bold",
                size=20,
                color="#ffffff"
            ))
        fig.update_layout(
            legend=dict(
                x=0.41,
                y=-0.1,
                traceorder="normal",
                font=dict(
                    family="sans-serif",
                    size=18,
                    color="white"
                )
            )
        )
    fig.update_layout(legend_orientation="h")
    filePath = './template/resource/' + prov + '_pie_word_positivity.html'
    if os.path.exists(filePath):
        os.remove(filePath)

    fig.write_html("./template/resource/" + prov + "_pie_word_positivity.html")
    return "./template/resource/" + prov + "_pie_word_positivity.html"

def create_graph(prov,data):
    data.sort(reverse=True)
    dataframe = pd.DataFrame({'id':range(1,len(data)+1), 'positivity':[int(round(element,2)*100) for element in data]})
    fig = px.bar(dataframe.sort_values(by=['positivity']), x='id', y='positivity',
    color='positivity',
    labels={'positivity':'value positivity'}, height=400 , color_continuous_scale=[(0, "red"), (0.5, "orange"), (1, "green")])
    fig.update_layout({'plot_bgcolor': 'rgba(0, 0, 0, 0)','paper_bgcolor': 'rgba(0, 0, 0, 0)'})
    fig.update_layout(
        xaxis_title="Id element",
        title={
            'text': "Global Positivity",
            'y': 0.9,
            'x': 0.5,
            'xanchor': 'center',
            'yanchor': 'top'},

        font=dict(
            family="san-serif, bold",
            size=20,
            color="#ffffff"
        ))

    filePath = './template/resource/'+prov+'_global_positivity.html'
    if os.path.exists(filePath):
        os.remove(filePath)

    fig.write_html("./template/resource/"+prov+"_global_positivity.html")
    return "./template/resource/"+prov+"_global_positivity.html"
