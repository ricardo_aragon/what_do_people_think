import unittest
from server import app

from flask_testing import TestCase

class MyTest(unittest.TestCase):

	def test_home(self):
		tester = app.test_client(self)
		response = tester.get('/', content_type='html/text')
		self.assertEqual(response.status_code, 200)
		self.assertEqual(response.data != b'null', True)

	def test_other(self):
		tester = app.test_client(self)
		response = tester.get('a', content_type='html/text')
		self.assertEqual(response.status_code, 200)
		self.assertTrue(b'does not exist' in response.data)	
		
	def test_entryPoint_amazon(self):
		tester = app.test_client(self)
		response = tester.get('/get-text', data = {'keyword':'Iphone', 'provider':'amazon', 'quantity':'15'})
		self.assertEqual(response.status_code, 200)
		self.assertEqual(response.data != b'null', True)
	
	def test_entryPoint_twitter(self):
		tester = app.test_client(self)
		response = tester.get('/get-text', data = {'keyword':'Iphone', 'provider':'twitter', 'quantity':'15'})
		self.assertEqual(response.status_code, 200)
		self.assertEqual(response.data != b'null', True)
	
	def test_entryPoint_all_provider(self):
		tester = app.test_client(self)
		response = tester.get('/get-text', data = {'keyword':'Iphone', 'provider':'all', 'quantity':'15'})
		self.assertEqual(response.status_code, 200)
		self.assertEqual(response.data != b'null', True)

		
if __name__ == '__main__':
	unittest.main()