import unittest
import os.path
from scraper_tweets import scraper
from scraper_tweets import save_json

class ClassTestTweets(unittest.TestCase):
	def test_keyword_scraper(self):
		tweet, code, word = scraper("iPhone")
		self.assertEqual(word, "iPhone")
		self.assertEqual(code, 0)
		
	def test_tweets_scraper(self):
		tweet, code, word = scraper("Tv",5)
		self.assertEqual(len(tweet), 5)
	
	def test_args_default(self):
		tweet, code, word = scraper()
		self.assertEqual(word, "cuffie")
		self.assertEqual(len(tweet) <= 20 , True)
		self.assertEqual(code, 0)
		
	def test_number_fields(self):
		tweet, code, word = scraper("pc", 5)
		self.assertEqual(code, 0)
		
		for i in tweet:
			self.assertEqual(len(i),4)

	def test_not_null_field(self):
		tweet, code, word = scraper()
		self.assertEqual(code, 0)
		for i in tweet:
			self.assertIsNot(i["ID_user"],"")
			self.assertIsNot(i["ID_elemento"],"")
			self.assertIsNot(i["content"],"")
			self.assertIsNot(i["provider"],"")
			
	def test_dont_create_file(self):
		_, _, _ = scraper("mouse", 4)
		self.assertFalse(os.path.isdir('./comments'))

	
	def test_error_code(self):
		tweet, code, word = scraper("hstgfnhngfgdr")
		self.assertEqual(word, "null")
		self.assertEqual(code, 1)	
			

if __name__ == '__main__':
	unittest.main()