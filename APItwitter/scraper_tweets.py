#install = pip install GetOldTweets3
import logging
import os
import sys
import json
if sys.version_info[0] < 3:
    raise Exception("Python 2.x is not supported. Please upgrade to 3.x")

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))

import GetOldTweets3 as got

#cuffie 	
def scraper(keyword ='cuffie', count = 20):

	for handler in logging.root.handlers[:]:
		logging.root.removeHandler(handler)

	logging.basicConfig(format='', level=logging.INFO)
	logging.info("\n\n")
	for handler in logging.root.handlers[:]:
		logging.root.removeHandler(handler)
	logging.basicConfig(format='TWITTER %(asctime)s - %(levelname)s - %(message)s', level=logging.INFO)

	logging.info('SEARCH = {}'.format(keyword))
	# Creation of query object
	tweetCriteria = got.manager.TweetCriteria().setQuerySearch(keyword).setLang('it').setMaxTweets(count)
										   
											
	# Creation of list that contains all tweets
	tweets = got.manager.TweetManager.getTweets(tweetCriteria)
	# Creating list of chosen tweet data
	user_tweets = [[tweet.id, tweet.username, tweet.text] for tweet in tweets]
	json_tweets = [{"provider":"Twitter", "ID_user" : tweet.username, "ID_elemento": tweet.id, "content": tweet.text}
					for tweet in tweets]
					
	#print(json.dumps({"tweets": json_tweets}, indent = 3))
	#for i in user_tweets:
	#	print(i)
	logging.info("Number reviews obtained: "+str(len(json_tweets)))
	if (len(json_tweets) == 0):
		# logging.info("Not found tweets code 1")
		return 'null',1, 'null'
	# logging.info("Found tweets code 0")
	# save_json(json_tweets, keyword)
	return json_tweets,0, keyword

def save_json(json_tweets, keyword):
	with open('comments/twitter_'+keyword+'.json', 'w', encoding='utf-8') as outfile:
		json.dump(json_tweets, outfile, sort_keys=True, indent=4, ensure_ascii=False)
	
def main():
	tweets, code, keyword = scraper()
	if code == 1:
		#not found tweets 
		return code
	# save_json(tweets, keyword)
	#found tweets
	return code
	
if __name__ == "__main__":
	main()