# HTML

## Obiettivo
La pagina HTML consente l'inserimento di una keyword per la ricerca e la selezione di un provider. 

## Descrizione
* input : inserimento keyword e selezione nome provider
* output : keyword e nome provider
* run : index.html

## Flusso di esecuzione
Si utilizza una 'text box' per l'inserimento della keyword e una 'list box' per la selezione del
provider. E' stato utilizzato un bottone, che invia una richiesta di tipo POST a http://localhost:5000/get-text,
per avviare l'esecuzione del sistema. Si realizzano anche controlli sulla keyword inserita utilizzando 
espressioni regolari.