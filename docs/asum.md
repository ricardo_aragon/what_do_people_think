## ASUM

## Obiettivo
ASUM corrisponde ad un modello di aspect sentiment in grado di ritornare informazioni di probabilità che riguardano topic/sentiment dei documenti analizzati  

## Descrizione
* input : BagOfSentences.txt, SentiWords-0.txt, WordList.txt
* output : Phi.csv, Pi.csv, ProbWords.csv, Theta.csv
* run : java -jar ASUM/asum-master/target/asum-1.0.1-SNAPSHOT.jar

## Flusso di esecuzione
Una volta creati i file BagOfSentences.txt e WordList.txt è possibile richiamare l'applicativo ASUM per ricevere in output i file necessari. Il relativo jar si richiama da all'interno di apiServer/server.py ed attende il completamento dell'esecuzione. 
<br>La folder ASUM contiene anche due script che si occupano del processing:
* *pre_process.py*: crea i file BagOfSentences.txt e WordList.txt partendo dalle recensioni trovate dalle api. Le recensioni sono tokenizzate, ciascuna parola lemmatizzata rimuovendo le stop-word e tenendo conto della punteggiatura e numeri.
* *post_process.py*: la funzione principale **operations** resituisce un dizionario:
    * Key *global_sentiment_per_tweet*: restituisce la positività per ciascun tweets
    * Key *tweets_word*: restituisce, per ogni parola in *wordList.txt*, il numero di istanze che possiedono quella parola, il numero di volte che tale parola è presente in tutte le istanze, il numero totale di parole e il numero totale di istanze.
    * Function *word_positive*: calcola per ogni parola la positività associata