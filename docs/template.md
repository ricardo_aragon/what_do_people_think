# Template

## Obiettivo
La folder tempalte contiene tutte le risorse necessarie per la visualizzazione delle pagine HTML ed i relativi test Selenium. 

## Descrizione
* resource: riguardano le pagine HTML utilizzate per creare le visualizzazioni dinamiche
* root: vi è la pagina principale index.html e tutte le pagine di collegamento ad essa.

## Flusso di esecuzione
L'esecuzione parte da index.html. Successivamente si richiama da server la pagina select_view.html per visualizzare i risultati. In base al tipo di visualizzazione scelta si chiamano le relative pagine con i parametri che specificano la configurazione scelta dall'utente.