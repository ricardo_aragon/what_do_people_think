# WHAT DO PEOPLE THINK?
> Il progetto si basa sullo sviluppo di un sistema in grado di restituire una distribuzione di probabilità "sentiment/aspect" di documenti (in ITALIANO) recuperati da diverse fonti, per poter effettuare analisi legate a qualità e soddisfazione sugli articoli analizzati. <br/>**Nello specifico:**<br/> Il sistema sviluppato e, successivamente, gestito da Docker sarà costituito da un API che si occuperà di estrarre automaticamente, secondo diverse keywords specificate dal committente, le informazioni provenienti provenienti da Tweets, o recensioni provenienti da prodotti Amazon. Successivamente, un applicativo intermedio si occuperà di convertire i documenti recuperati dall'API in un formato compatibile con il modello di apprendimento non-supervisionato di aspect/sentiment. Quest'ultimo, dopo essere opportunamente configurato, restituirà la relativa topic/sentiment probability.

## Architettura
![](images/architecture.jpg)

## Documentazione
[Descrizione delle funzionalita][1]

[1]: https://ricardo_aragon.gitlab.io/what_people_think/

## Prerequisiti

* Python 3.7 o superiore
* Java jdk 12

## Installazione
```sh
$ git clone https://gitlab.com/ricardo_aragon/what_do_people_think.git

```

## Setup primo avvio
Dalla root della repository
```sh
$ pip install -r requirements.txt
$ python apiServer/server.py
```

## Avvio app
Dalla root della repository
```sh
$ python apiServer/server.py
```

## Risorse
* Pagina principale: localhost:5000

## Note
* Il sistema supporta solo la lingua ITALIANA.<br>
* Tutti i logs sono stampati in console. In caso di errori, aprire una issue.
* Se si eseguono troppe richieste, il provider Amazon potrà restituire 0 recensioni a causa della configurazione di Amazon.it.

## Autori

* [**Mirko Agarla**][2]  (806951)
* [**Ricardo Matamoros**][3] (807450)

[2]: https://gitlab.com/cimice15
[3]: https://gitlab.com/ricardo_aragon