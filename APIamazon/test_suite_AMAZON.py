import unittest
import os.path
from core_extract_comments import get_comments_based_on_keyword

class ClassTestTweets(unittest.TestCase):

	def test_tweets_scraper(self):
		keyword ="samsung s10"
		reviews,code = get_comments_based_on_keyword(keyword)
		if code==1:
			self.assertTrue(reviews==[])
		else:
			self.assertTrue(len(reviews)>= 5)

	def test_number_fields(self):
		keyword ="tv oled samsung"
		reviews,code = get_comments_based_on_keyword(keyword)
		if code==1:
			self.assertTrue(reviews==[])
		else:
			for i in reviews:
				self.assertEqual(len(i),4)

	def test_not_null_field(self):
		keyword = "chitarra"
		reviews,code = get_comments_based_on_keyword(keyword)
		if code==1:
			self.assertTrue(reviews==[])
		else:
			for i in reviews:
				self.assertIsNot(i["ID_user"],"")
				self.assertIsNot(i["ID_elemento"],"")
				self.assertIsNot(i["content"],"")

	def test_no_file_creation(self):
		keyword="mouse wifi"
		reviews, code= get_comments_based_on_keyword(keyword)


	def test_code(self):
		keyword = "mouse wifi"
		reviews,code= get_comments_based_on_keyword(keyword)
		if code==1:
			self.assertTrue(reviews==[])
		else:
			self.assertTrue(code==0)
		keyword = "weiohfgdhnr3"
		_, code = get_comments_based_on_keyword(keyword)
		self.assertTrue(code == 1)
		
	def test_duplicates(self):
		keyword = "samsung s20"
		reviews,code= get_comments_based_on_keyword(keyword)
		if code==1:
			self.assertTrue(reviews==[])
		else:
			duplicates=False
			for r1 in range(len(reviews)):
				cont=reviews[r1]['content']
				for r2 in range(len(reviews)):
					if reviews[r2]['content']==cont and r1!=r2:
						duplicates=True
					if duplicates==True:
						break
				if duplicates == True:
					break
			self.assertFalse(duplicates)

if __name__ == '__main__':
	unittest.main()