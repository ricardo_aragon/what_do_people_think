
import math
import re
import textwrap
import logging
import itertools
from constants import AMAZON_BASE_URL
from core_utils import get_soup

# https://www.amazon.co.jp/product-reviews/B00Z16VF3E/ref=cm_cr_arp_d_paging_btm_1?ie=UTF8&reviewerType=all_reviews&showViewpoints=1&sortBy=helpful&pageNumber=1

def get_product_reviews_url(item_id, page_number=None):
    if not page_number:
        page_number = 1
    return AMAZON_BASE_URL + '/product-reviews/{}/ref=' \
                             'cm_cr_arp_d_paging_btm_1?ie=UTF8&reviewerType=all_reviews' \
                             '&showViewpoints=1&sortBy=helpful&pageNumber={}'.format(
        item_id, page_number)


def get_comments_based_on_keyword(search, MaxReviews=50):

    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)

    logging.basicConfig(format='', level=logging.INFO)
    logging.info("\n\n")
    for handler in logging.root.handlers[:]:
        logging.root.removeHandler(handler)

    logging.basicConfig(format='AMAZON %(asctime)s - %(levelname)s - %(message)s', level=logging.INFO)

    global numMaxReviews
    logging.info('SEARCH = {}'.format(search))
    url = AMAZON_BASE_URL + '/s/ref=nb_sb_noss_2?url=search-alias%3Daps&field-keywords=' + \
          search + '&rh=i%3Aaps%2Ck%3A' + search
    soup = get_soup(url)
    code = 1
    reviews = []
    if soup!=None:
        product_ids=[]
        for div in soup.find_all('div'):
            if 'data-asin' in div.attrs:
                product_ids.append(div.attrs['data-asin'])
        # product_ids = [div.attrs['data-asin'] for div in soup.find_all('div') if 'data-index' in div.attrs]
        logging.info('Found {} items.'.format(len(product_ids)))
        if MaxReviews != None:
            numMaxReviews = MaxReviews
        else:
            numMaxReviews=20
        allReviews=[]
        for product_id in product_ids:
            # logging.info('product_id is {}.'.format(product_id))
            reviews = get_comments_with_product_id(product_id)
            allReviews.append(reviews)
            if numMaxReviews<=0:
                break
        reviews = list(itertools.chain.from_iterable(allReviews))
        logging.info("Number reviews obtained: "+str(len(reviews)))
        if len(reviews)>0:
            code=0
        else:
            code=1
    return reviews, code


def get_comments_with_product_id(product_id):
    global numMaxReviews
    reviews = list()
    if product_id is None:
        return reviews
    if not re.match('^[A-Z0-9]{10}$', product_id):
        return reviews

    product_reviews_link = get_product_reviews_url(product_id)
    so = get_soup(product_reviews_link)
    if so!=None:
        max_page_number = so.find(attrs={'data-hook': 'total-review-count'})
        if max_page_number is None:
            return reviews
        # print(max_page_number.text)
        max_page_number = ''.join([el for el in max_page_number.text if el.isdigit()])
        # print(max_page_number)
        max_page_number = int(max_page_number) if max_page_number else 1

        max_page_number *= 0.1  # displaying 10 results per page. So if 663 results then ~66 pages.
        max_page_number = math.ceil(max_page_number)
        for page_number in range(1, max_page_number):
            if page_number > 1:
                product_reviews_link = get_product_reviews_url(product_id, page_number)
                so = get_soup(product_reviews_link)

            cr_review_list_so = so.find(id='cm_cr-review_list')

            if cr_review_list_so != None:
                reviews_list = cr_review_list_so.find_all('div', {'data-hook': 'review'})
                if len(reviews_list) > 0:
                    for review in reviews_list:
                        numMaxReviews=numMaxReviews-1
                        body = review.find(attrs={'data-hook': 'review-body'}).text.strip()
                        author_url = review.find(attrs={'data-hook': 'genome-widget'}).find('a', href=True)
                        review_url = review.find(attrs={'data-hook': 'review-title'}).attrs['href']
                        if author_url:
                            author_url = author_url['href'].strip()
                        #elimino dati non importanti da ID_user e ID_elemento
                        """
                        s = '^\/(\S+)\/(\S+)\/(\S+=\S+=\S+=)(\S+)'
                        s1 = '^\/(\S+)\/(\S+)\/(\S+\.)(\S+)\/(\S*)'
                        result_elemento = re.search(s, review_url)
                        review_id = result_elemento.group(4)
                        result_user = re.search(s1, author_url)
                        author_id = result_user.group(4)
                        """
                        reviews.append({'provider':'Amazon','ID_user': author_url,'ID_elemento': review_url ,'content': body})
                        if (numMaxReviews <= 0):
                            break
                    if (numMaxReviews <= 0):
                        break
        return reviews


if __name__ == '__main__':
    get_comments_based_on_keyword("samsung")
