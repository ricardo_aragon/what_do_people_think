from collections import defaultdict
import spacy

def recDict():
    return defaultdict(recDict)

def global_positive():
    piFile = open("./ASUM/asum-master/output/Pi.csv","r", encoding="utf-8")
    lines=piFile.read().split("\n")
    global_sentiment_per_tweet=[float(r.split(",")[0]) for r in lines if r!=""]
    piFile.close()

    return global_sentiment_per_tweet

def word_positive():

    s0WordsFile = open("./ASUM/asum-master/input/SentiWords-0.txt", "r", encoding="utf-8")
    s0Words= s0WordsFile.read().split("\n")
    s1WordsFile = open("./ASUM/asum-master/input/SentiWords-1.txt", "r", encoding="utf-8")
    s1Words = s1WordsFile.read().split("\n")
    phiFile = open("./ASUM/asum-master/output/Phi.csv", "r", encoding="utf-8")
    lines = phiFile.read().split("\n")
    with open("./ASUM/resource/stop_word.txt", encoding="utf-8") as f:
        stop_word = [line.rstrip('\n') for line in f]
    positive_sentiment_per_word={}
    for i in range(1,len(lines)):
        w=lines[i].split(",")[0]
        if not w in s0Words and not w in s1Words and w not in stop_word:
            s0=float(lines[i].split(",")[1])
            s1 = float(lines[i].split(",")[2])
            positive_sentiment_per_word[w]=s0/(s0+s1)
    phiFile.close()
    s0WordsFile.close()
    s1WordsFile.close()
    return positive_sentiment_per_word

def number_tweets_per_word():
    wListFile = open("./ASUM/asum-master/input/WordList.txt", "r", encoding="utf-8")
    wList = wListFile.read().split("\n")
    bosFile = open("./ASUM/asum-master/input/BagOfSentences.txt", "r", encoding="utf-8")
    bos = bosFile.read().split("\n")
    numTweets=recDict()
    totalWords=0
    for i in range(1, len(bos), 2):
        elements=bos[i].split(" ")
        elements = list(filter(None, elements))
        for e in elements:
            e=int(e)
            totalWords+=1
            try:
                numTweets["word_total_in_tweet"][wList[e]]+=1
            except:
                numTweets["word_total_in_tweet"][wList[e]]=1
        elementsNoDup = set(elements)
        for e in elementsNoDup:
            e = int(e)
            try:
                numTweets["num_tweet_has_word"][wList[e]] += 1
            except:
                numTweets["num_tweet_has_word"][wList[e]] = 1

    numTweets["total_words"]=totalWords
    wListFile.close()
    bosFile.close()
    return numTweets

def getInfoWord(word, numTweets, totTweets):
    nlp = spacy.load("it_core_news_sm")
    lem_word = nlp(word)[0].lemma_.lower()
    if lem_word in numTweets["num_tweet_has_word"].keys():
        pos=word_positive()[lem_word]
        #word total in all tweets, num tweet has word, total_tweets, total_words
        return[numTweets["word_total_in_tweet"][lem_word], numTweets["num_tweet_has_word"][lem_word],totTweets, numTweets["total_words"],pos]
    else:
        return None


def operations():
    results={}
    results["global_sentiment_per_tweet"]=global_positive()
    results["tweets_word"] = number_tweets_per_word()
    return results

