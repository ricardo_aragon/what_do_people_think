import unittest
import os.path
import sys

sys.path.append('./APItwitter')
from scraper_tweets import scraper
from pre_process import preprocess
import os.path
import time
from subprocess import Popen
from post_process import operations, word_positive

class ClassTestTweets(unittest.TestCase):
	def test_duplicates_word_list(self):
		reviews, code, _ = scraper("iPhone", 10)
		preprocess(reviews)
		bowFile = open("./ASUM/asum-master/input/WordList.txt", "r")
		words=bowFile.read().split("\n")
		if len(words)==len(set(words)):
			duplicates=False
		else:
			duplicates=True
		bowFile.close()
		self.assertFalse(duplicates)
		
	def test_void_word_list(self):
		reviews, code, _ = scraper("iPhone", 10)
		preprocess(reviews)
		bowFile = open("./ASUM/asum-master/input/WordList.txt", "r")
		words = bowFile.read().split("\n")
		if len(words)>0:
			void = False
		else:
			void = True
		bowFile.close()
		self.assertFalse(void)

	def test_indexes_bos(self):
		reviews, code, _ = scraper("iPhone", 10)
		preprocess(reviews)
		bowFile = open("./ASUM/asum-master/input/WordList.txt", "r")
		bosFile = open("./ASUM/asum-master/input/BagOfSentences.txt", "r")
		words = bowFile.read().split("\n")
		sent= bosFile.read().split("\n")
		error=False
		for s in range(1,len(sent),2):
			ind=sent[s].split(" ")
			for i in ind:
				if i!='' and int(i)>=len(words):
					error=True
			if error==True:
				break
		# remove dummy files
		files=os.listdir("./ASUM/asum-master/output")
		for f in files:
			if f != "removeMe":
				os.remove(os.path.join("./ASUM/asum-master/output",f))
		bowFile.close()
		bosFile.close()
		self.assertFalse(error)

	def test_duplicates_Positive_sentiment_words(self):
		bowFile = open("./ASUM/asum-master/input/SentiWords-0.txt", "r")
		words=bowFile.read().split("\n")
		if len(words)==len(set(words)):
			duplicates=False
		else:
			duplicates=True
		bowFile.close()
		self.assertFalse(duplicates)

	def test_duplicates_Negative_sentiment_words(self):
		bowFile = open("./ASUM/asum-master/input/SentiWords-1.txt", "r")
		words=bowFile.read().split("\n")
		if len(words)==len(set(words)):
			duplicates=False
		else:
			duplicates=True
		bowFile.close()
		self.assertFalse(duplicates)

	def test_uniqueness_sentiment_words(self):
		uniqueness=True
		bow0File = open("./ASUM/asum-master/input/SentiWords-0.txt", "r")
		bow1File = open("./ASUM/asum-master/input/SentiWords-1.txt", "r")
		for w in bow0File:
			if w in bow1File:
				uniqueness=False
		bow0File.close()
		bow1File.close()
		self.assertTrue(uniqueness)

	def test_creation_files_phi(self):
		reviews, code, _ = scraper("iPhone", 10)
		preprocess(reviews)
		proc = Popen("java -jar ./ASUM/asum-master/asum-1.0.1-SNAPSHOT.jar", shell=True)
		proc.wait()
		proc.kill()
		exist=False
		files=os.listdir("./ASUM/asum-master/output")
		for f in files:
			if f.endswith("Phi.csv"):
				exist=True
			if f != "removeMe":
				os.remove(os.path.join("./ASUM/asum-master/output",f))
		self.assertTrue(exist)

	def test_creation_files_theta(self):
		reviews, code, _ = scraper("iPhone", 10)
		preprocess(reviews)
		proc = Popen("java -jar ./ASUM/asum-master/asum-1.0.1-SNAPSHOT.jar", shell=True)
		proc.wait()
		proc.kill()
		exist = False
		files = os.listdir("./ASUM/asum-master/output")
		for f in files:
			if f.endswith("Theta.csv"):
				exist = True
			if f != "removeMe":
				os.remove(os.path.join("./ASUM/asum-master/output",f))
		self.assertTrue(exist)

	def test_creation_files_pi(self):
		reviews, code, _ = scraper("iPhone", 10)
		preprocess(reviews)
		proc = Popen("java -jar ./ASUM/asum-master/asum-1.0.1-SNAPSHOT.jar", shell=True)
		proc.wait()
		proc.kill()
		exist = False
		if os.path.isdir("./ASUM/asum-master/output"):
			files = os.listdir("./ASUM/asum-master/output")
			for f in files:
				if f.endswith("Pi.csv"):
					exist = True
				if f != "removeMe":
					os.remove(os.path.join("./ASUM/asum-master/output",f))
		self.assertTrue(exist)

	def test_creation_files_probw(self):
		reviews, code, _ = scraper("iPhone", 10)
		preprocess(reviews)
		proc = Popen("java -jar ./ASUM/asum-master/asum-1.0.1-SNAPSHOT.jar", shell=True)
		proc.wait()
		proc.kill()
		exist = False
		if os.path.isdir("./ASUM/asum-master/output"):
			files = os.listdir("./ASUM/asum-master/output")
			for f in files:
				if f.endswith("ProbWords.csv"):
					exist = True
				if f != "removeMe":
					os.remove(os.path.join("./ASUM/asum-master/output",f))
		self.assertTrue(exist)

	def test_post_process_global_list_length(self):
		reviews, code, _ = scraper("iPhone", 10)
		preprocess(reviews)
		proc = Popen("java -jar ./ASUM/asum-master/asum-1.0.1-SNAPSHOT.jar", shell=True)
		proc.wait()
		proc.kill()
		results= operations()
		results=results["global_sentiment_per_tweet"]
		sameLength=False
		if len(results)==len(reviews):
			sameLength=True
		self.assertTrue(sameLength)

	def test_post_process_global_list_probValues(self):
		reviews, code, _ = scraper("iPhone", 10)
		preprocess(reviews)
		proc = Popen("java -jar ./ASUM/asum-master/asum-1.0.1-SNAPSHOT.jar", shell=True)
		proc.wait()
		proc.kill()
		results= operations()
		results = results["global_sentiment_per_tweet"]
		outOfBounds=False
		for r in results:
			if float(r)<0 or float(r)>1:
				outOfBounds=True
		self.assertFalse(outOfBounds)

	def test_post_process_word_pos_value(self):
		reviews, code, _ = scraper("iPhone", 10)
		preprocess(reviews)
		proc = Popen("java -jar ./ASUM/asum-master/asum-1.0.1-SNAPSHOT.jar", shell=True)
		proc.wait()
		proc.kill()
		results = word_positive()
		outOfBounds=False
		for word in results.keys():
			if results[word]<0 or results[word]>1:
				outOfBounds=True
		self.assertFalse(outOfBounds)

	def test_post_process_word_pos_not_presence(self):
		s0WordsFile = open("./ASUM/asum-master/input/SentiWords-0.txt", "r", encoding="utf-8")
		s0Words=s0WordsFile.read().split("\n")
		s1WordsFile = open("./ASUM/asum-master/input/SentiWords-1.txt", "r", encoding="utf-8")
		s1Words = s1WordsFile.read().split("\n")
		with open("./ASUM/resource/stop_word.txt", encoding="utf-8") as f:
			stop_word = [line.rstrip('\n') for line in f]
		reviews, code, _ = scraper("iPhone", 10)
		preprocess(reviews)
		proc = Popen("java -jar ./ASUM/asum-master/asum-1.0.1-SNAPSHOT.jar", shell=True)
		proc.wait()
		proc.kill()
		results = word_positive()
		presence=False
		for word in results.keys():
			if word in s0Words or word in s1Words or word in stop_word:
				presence = False
		s0WordsFile.close()
		s1WordsFile.close()
		self.assertFalse(presence)

	def test_post_process_word_pos_wordList_presence(self):
		wListFile = open("./ASUM/asum-master/input/WordList.txt","r", encoding="utf-8")
		wList= wListFile.read().split("\n")
		reviews, code, _ = scraper("iPhone", 10)
		preprocess(reviews)
		proc = Popen("java -jar ./ASUM/asum-master/asum-1.0.1-SNAPSHOT.jar", shell=True)
		proc.wait()
		proc.kill()
		results=word_positive()
		presence = True
		for word in results.keys():
			if word not in wList:
				presence = False
		wListFile.close()
		self.assertTrue(presence)

	def test_length_wList_totalWord_tweets(self):
		wListFile = open("./ASUM/asum-master/input/WordList.txt", "r", encoding="utf-8")
		wList = wListFile.read().split("\n")
		reviews, code, _ = scraper("iPhone", 10)
		preprocess(reviews)
		proc = Popen("java -jar ./ASUM/asum-master/asum-1.0.1-SNAPSHOT.jar", shell=True)
		proc.wait()
		proc.kill()
		results = operations()
		results1 = results["tweets_word"]["word_total_in_tweet"]
		results2 = results["tweets_word"]["num_tweet_has_word"]
		wListFile.close()
		self.assertTrue(len(results1)== len(wList)-1 and len(results2)== len(wList)-1)

if __name__ == '__main__':
	unittest.main()