from nltk.tokenize import RegexpTokenizer
import spacy
from spacy.cli import download

# setting up preprocessing
download("it_core_news_sm")
with open("./ASUM/resource/stop_word.txt", encoding="utf-8") as f:
    stop_word = [line.rstrip('\n') for line in f]
tokenizer = RegexpTokenizer('\w+|\$[\d]+|\w+')
nlp = spacy.load("it_core_news_sm")

def preprocess(reviews):
    wListFile = open("./ASUM/asum-master/input/WordList.txt","w", encoding="utf-8")
    bosFile = open("./ASUM/asum-master/input/BagOfSentences.txt", "w", encoding="utf-8")
    word_list = []
    indexes=[]
    ind=0
    for r in reviews:
        if isinstance(r,dict) and r != []:
            writed = False
            r=r["content"]
            # tokenize with regex
            filtered_words = tokenizer.tokenize(r.lower())
            # lemmatizzazione
            for i in filtered_words:
                lem_word= nlp(i)[0].lemma_
                if lem_word not in stop_word and i not in stop_word:
                    if not writed:
                        bosFile.write("1\n")
                        writed=True
                    if lem_word in word_list:
                        index=word_list.index(lem_word)
                    else:
                        index=len(word_list)
                        word_list.append(lem_word)
                        wListFile.write(lem_word+"\n")
                    bosFile.write(str(index)+" ")
        if writed:
            bosFile.write("\n")
        else:
            indexes.append(ind)
        ind=ind+1
    bosFile.close()
    wListFile.close()
    return word_list, indexes
