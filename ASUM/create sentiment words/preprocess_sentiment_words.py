import spacy
from spacy.cli import download



# setting up preprocessing
download("it_core_news_sm")

nlp = spacy.load("it_core_news_sm")
s0out = open("../asum-master/input/SentiWords-0.txt","w", encoding="utf-8")
s1out = open("../asum-master/input/SentiWords-1.txt", "w", encoding="utf-8")
s0 = open("aSentiWords-0.txt","r", encoding="utf-8").read().split("\n")
s1 = open("aSentiWords-1.txt","r", encoding="utf-8").read().split("\n")
sent0=[]
sent1=[]
for w in s0:
		sent0.append(nlp(w)[0].lemma_)
for w in s1:
		sent1.append(nlp(w)[0].lemma_)

sent0=set(sent0)
sent1=set(sent1)
for s in sent0:
	s0out.write(s+"\n")
for s in sent1:
	s1out.write(s+"\n")
s0out.close()
s1out.close()
