import spacy

nlp = spacy.load("it_core_news_sm")
stop_word = [line.rstrip('\n') for line in open("./ASUM/resource/stop_word_no_lemma.txt", encoding="utf-8")]

stwLemma=set([nlp(i)[0].lemma_ for i in stop_word])

bosFile = open("./ASUM/resource/stop_word.txt", "w", encoding="utf-8")

for stw in stwLemma:
    bosFile.write(stw+"\n")

bosFile.close()